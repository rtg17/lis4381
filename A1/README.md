# LIS4381
## Rhett Gordon
### A1 - Software Setup


### images

![ampps is ok](ammps.png)

![jdk works](helloworld.png)

![android works](android.png)

### git command descriptions

1. git init - Initializes the current directory (or a path to a directory, passed through an argument) as a local repository
2. git status - Allows one to see in the directory what will and won't be pushed into a repo when performing a commit
3. git add - Allows one to add files to the index to be commited later
4. git commit - Slaps all the changes that were indexed to the repository
5. git push - Updates remote refs alongside local ones
6. git pull - Fetches from another repo to integrate into either a local branch or another remote repo
7. git merge - Slaps two branches of a repo together, usually the master with a branch stemming from it

[bitbucket station locations](https://bitbucket.org/rtg17/bitbucketstationlocations)