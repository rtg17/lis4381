# LIS4381
## Rhett Gordon
### A2 - Healthy Recipes
Specs: Design a simple program to showcase a click event in Android Studio, in order to switch from one activity to another.

![Main activity](screenone.png)

![Recipe activity](screentwo.png)