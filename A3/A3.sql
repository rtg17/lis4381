-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema A3
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `A3` ;

-- -----------------------------------------------------
-- Schema A3
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `A3` DEFAULT CHARACTER SET utf8 ;
USE `A3` ;

-- -----------------------------------------------------
-- Table `A3`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `A3`.`petstore` ;

CREATE TABLE IF NOT EXISTS `A3`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `A3`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `A3`.`customer` ;

CREATE TABLE IF NOT EXISTS `A3`.`customer` (
  `cst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cst_fname` VARCHAR(20) NOT NULL,
  `cst_lname` VARCHAR(40) NOT NULL,
  `cst_street` VARCHAR(45) NOT NULL,
  `cst_city` VARCHAR(20) NOT NULL,
  `cst_state` CHAR(2) NOT NULL,
  `cst_zip` INT(9) UNSIGNED NOT NULL,
  `cst_phone` BIGINT UNSIGNED NOT NULL,
  `cst_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cst_total_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `cst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cst_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `A3`.`pets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `A3`.`pets` ;

CREATE TABLE IF NOT EXISTS `A3`.`pets` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `petstore_pst_id` SMALLINT UNSIGNED NOT NULL,
  `customer_cst_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(45) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pets_petstore_idx` (`petstore_pst_id` ASC),
  INDEX `fk_pets_customer1_idx` (`customer_cst_id` ASC),
  CONSTRAINT `fk_pets_petstore`
    FOREIGN KEY (`petstore_pst_id`)
    REFERENCES `A3`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pets_customer1`
    FOREIGN KEY (`customer_cst_id`)
    REFERENCES `A3`.`customer` (`cst_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
