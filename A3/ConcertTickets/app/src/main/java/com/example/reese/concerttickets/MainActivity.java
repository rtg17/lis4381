package com.example.reese.concerttickets;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    double costPerTicket;
    int numberOfTickets;
    double totalCost;
    String groupChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText tickets = (EditText)findViewById(R.id.txtTickets);
        final Spinner group = (Spinner)findViewById(R.id.txtGroup);
        Button cost = (Button)findViewById(R.id.btnCost);
        cost.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override
            public void onClick(View v) {
                groupChoice = group.getSelectedItem().toString();
                switch (groupChoice) {
                    case "Van Halen":
                        costPerTicket = 59.99;
                        break;
                    case "Led Zepplin":
                        costPerTicket = 49.99;
                        break;
                    case "Linkin Park":
                        costPerTicket = 39.99;
                        break;
                    case "Droning Pool":
                        costPerTicket = 39.99;
                        break;
                }
                numberOfTickets = Integer.parseInt(tickets.getText().toString());
                totalCost = costPerTicket * numberOfTickets;
                NumberFormat nf = NumberFormat.getInstance(Locale.US);
                result.setText("Cost for " + groupChoice + " tickets is $" + nf.format(totalCost));
            }
        });
    }
}
