# LIS4381
## Rhett Gordon
### A3 - Pet Store Database + Concert Ticket App
Specs: Design a database in MySQL using a local server (powered by AAMPS), and an app that calculates the cost of concert tickets based on concert and amount of tickets

## MySQL Workbench ERD
![ERD](ERD.png)

[ERD](A3.mwb "ERD") | [SQL Statements](A3.sql "SQL statements")

## Android Application
![Default Display](screenone.png)

![Calcuation Display](screentwo.png)