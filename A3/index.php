<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta name="description" content="Repo of software coded during 2018 LIS4381 with Mark Jowett, PhD.">
		<meta name="author" content="Rhett Gordon">
    <link rel="icon" href="../favicon.ico">

		<title>LIS4381 - A3</title>
		<?php include_once("../css/include_css.php"); ?>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>

		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Design a MySQL database through an ERD and develop an Android Application which performs basic arithmetic.
				</p>

				<h4>MySQL ERD</h4>
				<img src="ERD.png" class="img-responsive center-block" alt="MySQL ERD">

				<h4>Concert Ticket App</h4>
				<img src="concert.png" class="img-responsive center-block" alt="Concert Ticket App">

				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->
		<?php include_once("../js/include_js.php"); ?>
  </body>
</html>
