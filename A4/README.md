# LIS4381
## Rhett Gordon
### A4 - Client-side Validation & Website Setup
Specs: Set up portfolio website for previous assignments + perform client-side validation via regex and Bootstrap 

[Main page](../index.php)

##Images
![Main Page](images/mainpage.png)

![A4 - Blank State](images/A4-blankstate.png)

![A4 - Valid State](images/A4-validstate.png)

![A4 - Invalid State](images/A4-invalidstate.png)