# LIS4381
## Rhett Gordon
### A5 - Server-side Validation & MySQL DB connection
Specs: Connect to local MySQL DB and be able to view/add/delete/edit records

[Main page](index.php)

##Images
![DB item display](img/display.png)

![DB addition error](img/error.png)