<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Repo of software coded during 2018 LIS4381 with Mark Jowett, PhD.">
    <meta name="author" content="Rhett Gordon">
    <link rel="icon" href="../favicon.ico">
	<title>LIS4381 - P2</title>
    <?php include_once("../css/include_css.php"); ?>
  </head>
  <body>
    <?php include_once("../global/nav.php"); ?>
    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <h1>P2 - Data Manipulation in PHP</h1>
                <p><strong>Description:</strong> Utilize PHP to pass MySQL statements that modify data</p>
            </div>
            <h2>Pet Stores</h2>
            <form id="edit_store" method="post" class="form-horizontal" action="process/edit_petstore_process.php">
                <?php
                require_once('global/connection.php');
                $pst_id_v = $_POST['pst_id'];
                $query =
                    "SELECT * FROM petstore
                     WHERE pst_id = :pst_id_p";
                $statement = $db->prepare($query);
                $statement->bindParam(':pst_id_p', $pst_id_v);
                $statement->execute();
                $result = $statement->fetch();
                while($result != null) {
                    ?>
                    <input type="hidden" name="id" value="<?php echo $result['pst_id']; ?>"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="30" name="name"
                                   placeholder="PetstoreName"
                                   value="<?php echo $result['pst_name']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Street:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="30" name="street"
                                   placeholder="1111 Example Rd"
                                   value="<?php echo $result['pst_street']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">City:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="30" name="city" placeholder="Oviedo"
                                   value="<?php echo $result['pst_city']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">State:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="2" name="state" placeholder="FL"
                                   value="<?php echo $result['pst_state']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Zip code:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="9" name="zip" placeholder="32766"
                                   value="<?php echo $result['pst_zip']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Phone #:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="10" name="phone" placeholder="8009991234"
                                   value="<?php echo $result['pst_phone']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="100" name="email"
                                   placeholder="address@email.com"
                                   value="<?php echo $result['pst_email']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">URL:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="100" name="url"
                                   placeholder="https://www.example.com"
                                   value="<?php echo $result['pst_url']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">YTD sales:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="11" name="ytd" placeholder="14.00"
                                   value="<?php echo $result['pst_ytd_sales']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Notes:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="255" name="notes"
                                   value="<?php echo $result['pst_notes']; ?>"/>
                        </div>
                    </div>
                    <?php
                    $result = $statement->fetch();
                    }
                    $db = null;
                    ?>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary" name="edit" value="Edit">Submit</button>
                        </div>
                    </div>
            </form>
            <?php include_once "../global/footer.php"; ?>
        </div> <!-- starter-template -->
     </div> <!-- end container -->
    <!-- Bootstrap JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- <script src="../js/formValidation/formValidation.min.js" type="text/javascript"></script> -->
    <!-- <script src="../js/formValidation/bootstrap.min.js" type="text/javascript"></script> -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
    <!-- <script type="text/javascript">
        $(document).ready(function() {
            $('#edit_store').formValidation({
                message: 'This is not a valid value.',
                icon: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Name is required.'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'Name must be less than 30 characters long.'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9\-_\s]+$/,
                                message: 'Name can only contain letters, numbers, spaces, hyphens, and underscores.'
                            },
                        },
                    },
                    street: {
                        validators: {
                            notEmpty: {
                                message: 'Street required.'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'Street must be less than 30 characters long.'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9,\s\-\.]+$/,
                                message: 'Street can only contain letters, numbers, commas, or periods.'
                            },
                        },
                    },
                    city: {
                        validators: {
                            notEmpty: {
                                message: 'City required.'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'City name must be less than 30 characters long.'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9,\s\-]+$/,
                                message: 'City can only contain letters, numbers, commas, or periods.'
                            },
                        },
                    },
                    state: {
                        validators: {
                            notEmpty: {
                                message: 'State required.'
                            },
                            stringLength: {
                                min: 2,
                                max: 2,
                                message: 'Street must it\'s short-hand version (ex: FL).'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z]+$/,
                                message: 'State name can only contain letters.'
                            },
                        },
                    },
                    zip: {
                        validators: {
                            notEmpty: {
                                message: 'Zip required.'
                            },
                            stringLength: {
                                min: 5,
                                max: 9,
                                message: 'Zip code must be less than 9 characters long.'
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'Zip code can only by numbers.'
                            },
                        },
                    },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'Phone number required.'
                            },
                            stringLength: {
                                min: 10,
                                max: 10,
                                message: 'Phone number must be 10 characters long.'
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'Must be a valid American phone number.'
                            },
                        },
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email required.'
                            },
                            stringLength: {
                                min: 1,
                                max: 100,
                                message: 'Email must be less than 100 characters long.'
                            },
                            regexp: {
                                regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
                                message: 'Must be a valid email address.'
                            },
                        },
                    },
                    url: {
                        validators: {
                            notEmpty: {
                                message: 'URL required.'
                            },
                            stringLength: {
                                min: 1,
                                max: 100,
                                message: 'URL must be less than 100 characters long.'
                            },
                            regexp: {
                                regexp: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,
                                message: 'Must be a valid link to a website.'
                            },
                        },
                    },
                    ytd: {
                        validators: {
                            notEmpty: {
                                message: 'YTD required.'
                            },
                            stringLength: {
                                min: 1,
                                max: 11,
                                message: 'YTD must be within $999,999,999.99'
                            },
                            regexp: {
                                regexp: /^[0-9\.]+$/,
                                message: 'YTD must be a valid number amount'
                            },
                        },
                    },
                },
            });
        });
    </script> -->
    <!-- Placed at end of document so pages load faster -->
    <?php include_once("../js/include_js.php"); ?>
  </body>
</html>
