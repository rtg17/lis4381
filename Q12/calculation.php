<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Repo of software coded during 2018 LIS4381 with Mark Jowett, PhD.">
    <meta name="author" content="Rhett Gordon">
    <link rel="icon" href="../favicon.ico">
    <title>LIS4381 - Q12</title>
    <?php include_once("../css/include_css.php"); ?>
</head>
<body>
    <?php include_once("../global/nav.php"); ?>
    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("../global/header.php"); ?>
                <?php
                $numOne = $_POST['numOne'];
                $numTwo = $_POST['numTwo'];
                $method = $_POST['method'];
                #Display Operation
                echo "<h2>$method</h2>";
                /*if ($operation == 'Addition') {
                    $total = $num1 + $num2;
                    echo "$num1 + $num2 = $total";
                } elseif ($operation == 'Subtraction') {
                    $total = $num1 - $num2;
                    echo "$num1 - $num2 = $total";
                } elseif ($operation == 'Multiplication') {
                    $total = $num1 * $num2;
                    echo "$num1 x $num2 = $total";
                } elseif ($operation == 'Division') {
                    $total = $num1 / $num2;
                    echo "$num1 / $num2 = $total";
                } elseif ($operation == 'Exponents') {
                    $total = $num1 ** $num2;
                    echo "$num1 raised by $num2 = $total";
                }
                 */
                switch ($method) {
                    case "add": //Addition
                        $total = $numOne + $numTwo;
                        echo "$numOne plus $numTwo is $total";
                        break;
                    case "sub": //Subtraction
                        $total = $numOne - $numTwo;
                        echo "$numOne minus $numTwo is $total";
                        break;
                    case "mul": //Multiplication
                        $total = $numOne * $numTwo;
                        echo "$numOne times $numTwo is $total";
                        break;
                    case "div": //Division
                        //Check if division by zero is about to occur
                        if ($numTwo == 0) {
                        echo "Can not divide by 0"; 
                        } else {
                            $total = $numOne / $numTwo;
                            echo "$numOne divided by $numTwo is $total";
                        }
                        break;
                    case "exp": //Exponends
                        $total = $numOne ** $numTwo;
                        echo "$numOne to the power of $numTwo is $total";
                        break;
                }?>
                <div>
                    <br>
                    <form action="index.php" method="post">
                    <button type="submit" class="btn btn-primary" name="return" value="return">return</button>
                    <br>
                    <br>
                </div>
                <?php include_once("../global/footer.php"); ?> <!--required-->
            </div>
        </div> <!-- end starter-template -->
    </div> <!-- end container -->
</body>
</html>