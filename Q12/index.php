<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta name="description" content="Repo of software coded during 2018 LIS4381 with Mark Jowett, PhD.">
      <link rel="stylesheet" href="../css/formValidation.min.css">
		<meta name="author" content="Rhett Gordon">
    <link rel="icon" href="../favicon.ico">

		<title>LIS4381 - Q12</title>
		<?php include_once("../css/include_css.php"); ?>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Simple calculation with prevention of division by zero, switch statements, conditionals, and prevention of division by zero.
				</p>

				<!-- Write code for calc below -->
                <form id="calc" method="post" class="form-horizontal" action="calculation.php">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">First number:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="10" name="numOne" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Second number:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" maxlength="10" name="numTwo" />
                        </div>
                    </div>
                    <fieldset>
                        <p>
                            <input type = "radio"
                                   name = "method"
                                   id = "add"
                                   value = "add" />
                            <label for = "add">add</label>
                            <input type = "radio"
                                   name = "method"
                                   id = "sub"
                                   value = "sub" />
                            <label for = "sub">sub</label>
                            <input type = "radio"
                                   name = "method"
                                   id = "mul"
                                   value = "mul" />
                            <label for = "mul">mul</label>
                            <input type = "radio"
                                   name = "method"
                                   id = "div"
                                   value = "div" />
                            <label for = "div">div</label>
                            <input type = "radio"
                                   name = "method"
                                   id = "exp"
                                   value = "exp" />
                            <label for = "exp">exp</label>
                        </p>
                    </fieldset>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
                        </div>
                    </div>
                </form>
                <div id="result"></div>

                <script type="text/javascript">
                    //dont think i need this
                </script>


                <!-- Write code for calc above -->

				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->
		<?php include_once("../js/include_js.php"); ?>

        <script type="text/javascript">
            $('#calc').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    numOne: {
                        validators: {
                            notEmpty: {
                                message: 'Number required'
                            },
                            stringLength: {
                                min: 1,
                                max: 100000000000000,
                                message: 'Please enter a number'
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'Input must be a valid numeric value'
                            },
                        },
                    },
                    numTwo: {
                        validators: {
                            notEmpty: {
                                message: 'Number required'
                            },
                            stringLength: {
                                min: 1,
                                max: 100000000000000,
                                message: 'Please enter a number'
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'Input must be a valid numeric value'
                            },
                        },
                    },

                }
            });
            });
        </script>
  </body>
</html>
