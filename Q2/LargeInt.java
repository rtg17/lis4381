import java.util.Scanner;

public class LargeInt {
    public static void main(String[] args) {
        Scanner numOne = new Scanner(System.in);
        Scanner numTwo = new Scanner(System.in);

        System.out.println("Enter first num: ");
        int firstNum = numOne.nextInt();
        System.out.println("Enter second num: ");
        int secondNum = numTwo.nextInt();

        if (firstNum > secondNum) {
            System.out.println(firstNum + ">" + secondNum);
        } else if (secondNum > firstNum) {
            System.out.println(secondNum + ">" + firstNum);
        } else {
            System.out.println("Both are equal");
        }
    }
}