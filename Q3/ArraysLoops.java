public class ArraysLoops {
    public static void main(String[] args) {
        int i;
        String names[] = {"dog","cat","bird","fish","insect"};

        System.out.println("for loop:");
        for (i = 0; i < names.length; i++) {
            System.out.println(names[i]);            
        }

        i = 0; // reset
        System.out.println("\nenchnaced for loop:");
        for (String total: names) {
            System.out.println(total);
        } // I really do not like this loop
          // Its syntaxing is confusing and I don't really see much a practical use for this type of loop outside of population of arrays


        i = 0; // gotta reset
        System.out.println("\nwhile loop:");
        while (i < names.length) {
            System.out.println(names[i]);
            i++;
        }

        i = 0; // please reset again
        System.out.println("\ndo while loop:");
        do {
            System.out.println(names[i]);
            i++;
        } while (i < names.length);
    }
}