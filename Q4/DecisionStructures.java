import java.util.Scanner;

public class DecisionStructures {
    public static void main(String[] args) {
        String myStr;
        char myChar;
        Scanner sc = new Scanner(System.in);

        System.out.print("Phone types: W or w, C or c, H or h, N or n\nUse if-else and switch decision structures\nEnter a char: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.print("if-else\nPhone type: ");
        if (myChar == 'w') {
            System.out.print("work");
        } else if (myChar == 'c') {
            System.out.print("cell");
        } else if (myChar == 'h') {
            System.out.print("home");
        } else if (myChar == 'n') {
            System.out.print("none");
        } else {
            System.out.print("ERROR: Didn't enter valid entry");
        }

        System.out.print("\nswitch\nPhone type: ");
        switch (myChar) {
            case 'w':
                System.out.print("work");
                break;
            case 'c':
                System.out.print("cell");
                break;
            case 'h': 
                System.out.print("home");
                break;
            case 'n':
                System.out.print("none");
                break;
            default:
                System.out.print("ERROR: Didn't enter valid entry");
                break;
        }
    }
}