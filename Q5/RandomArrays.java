import java.util.Scanner;
import java.util.Random;

public class RandomArrays {
    public static void main(String[] args) {
        int numInput;
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();
        boolean inputFlag = false;
        int arraySize;
        int i;

        //ensure proper int input
        do {
            System.out.print("Program prompts user to enter desired number of pseudorandom-generated integers (min 1)\nUse following loop structures: For, enhanced for, while, do/while\n\nEnter desired number of pseudorandom-generated integers (min 1): ");
            arraySize = sc.nextInt();
            if (arraySize <= 0) {
                System.out.print("Not a valid input.\n");
            }
        } while (inputFlag = false);

        int myArray[] = new int[arraySize];

        System.out.print("\nfor loop:\n");
        for (i = 0; i < myArray.length; i++) {
            System.out.println(rand.nextInt());
        }

        System.out.print("\nenhanced for loop:\n");
        for (int j: myArray) {
            System.out.println(rand.nextInt());
        }

        System.out.print("\nwhile loop:\n");
        i = 0;
        while (i < myArray.length) {
            System.out.println(rand.nextInt());
            i++;
        }

        System.out.print("\ndo-while loop:\n");
        i = 0;
        do {
            System.out.println(rand.nextInt());
            i++;
        } while (i < myArray.length);
     
    }
}