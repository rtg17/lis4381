import java.util.Scanner;
import java.util.Random;

public class MethodArrays {
    public static void displayProgram() {
        System.out.print("Program prompts user to enter desired number of pseudorandom-generated integers (min 1)\nUse following loop structures: For, enhanced for, while, do/while\n\n");
    }
    
    public static int getArraySize() {
        int result;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        result = sc.nextInt();
        sc.close();
        return result;
    }

    public static void displayArrays(int in) {
        int i;
        Random rand = new Random();
        int myArray[] = new int[in];

        System.out.print("\nfor loop:\n");
        for (i = 0; i < myArray.length; i++) {
            System.out.println(rand.nextInt());
        }

        System.out.print("\nenhanced for loop:\n");
        for (int j: myArray) {
            System.out.println(rand.nextInt());
        }

        System.out.print("\nwhile loop:\n");
        i = 0;
        while (i < myArray.length) {
            System.out.println(rand.nextInt());
            i++;
        }

        System.out.print("\ndo-while loop:\n");
        i = 0;
        do {
            System.out.println(rand.nextInt());
            i++;
        } while (i < myArray.length);
    }

    public static void main(String[] args) {
        displayProgram();
        int arrayNum = getArraySize();
        displayArrays(arrayNum);
    }
}