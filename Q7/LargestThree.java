import java.util.Scanner;
import java.util.Arrays;

int SIZE = 3;

public class LargestThree {
  public static void populateArray(int[] inputs) {
    Scanner sc = new Scanner(System.in);
    boolean flag = false;
    for (int i = 0; i < inputs.length; i++) {
      while(!sc.hasNextInt()) {
        System.out.print("Input integer: ");
        sc.nextInt();
      }
      inputs[i] = sc.nextInt();
    }
    sc.close();
  }

  public static void displayHighest(int[] inputs) {
    //I apologize, I could not think of an elegant way to properly write this
    int dupe[] = new int[inputs.length];
    dupe = Arrays.copyOf(inputs, dupe.length);

    for (int i = 0; i < dupe.length - 1; i++) {
      for (int j = 0; j < dupe.length - i - 1; j++) {
        if (dupe[j] < dupe[j+1]) {
          int temp = dupe[j];
          dupe[j] = dupe[j+1];
          dupe[j+1] = temp;
        }
      }
    }

    for (int i = 0; i < dupe.length; i++) {
      if (dupe[0] == inputs[i]) {
        if (i == 0) {
          System.out.println("the first number is the highest.");
        } else if (i == 1) {
          System.out.println("the second number is the highest.");
        } else if (i == 2) {
          System.out.println("the third number is the highest.");
        } else {
          System.out.println("An out-of-hardcoded-range element is highest. Oops.");
        }
      }
    }
  }

  public static void main(String[] args) {
    int inputs[] = new int[SIZE];
    populateArray(inputs);
    displayHighest(inputs);
  }
}
