import java.util.Scanner;

public class RuntimeArray {
  public static void displayReqs() {
    System.out.println("1) Create an array at run-time");
    System.out.println("2) Display array size");
    System.out.println("3) Sound sum and avg to two decimal places");
    System.out.println("4) MUST be Float, not Double");
  }

  public static void displaySumAvg(float[] a) {
    float sum = 0;
    for (int i = 0; i < a.length; i++) {
      sum += a[i];
    }
    System.out.println("The sum is " + sum);
    System.out.printf("The avg is %.2f", (sum / a.length));
  }

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int size;

    displayReqs();
    System.out.print("Input array size: ");
    size = input.nextInt();
    float arr[] = new float[size];
    for (int i = 0; i < arr.length; i++) {
      System.out.printf("Enter num %d: ", (i + 1));
      arr[i] = input.nextFloat();
    }
    displaySumAvg(arr);
    input.close();
  }
}
