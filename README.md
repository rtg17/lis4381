<!-- For reference https://bitbucket.org/tutorials/markdowndemo/src/master/README.md -->

# LIS-4381 - Mobile Web App Development
## Rhett Gordon
### Reqs:
Course work links:

- [A1/README.md](A1/README.md "A1 README.md")

    - Install AMPPS/Show it can run

    - Install JDK/Run basic "Hello World" program

    - Install Android Studio/Show ability to compile basic application

    - Provide screenshots of above

    - Create Bitbucket repo

    - Complete Bitbucket tutotials (bitbucketstationlocations)

    - Provide git command descriptions

- [A2/README.md](A2/README.md "A2 README.md")

    - Create Android application utilizing onClick events and multiple activities

    - Utilize constraints to ensure layouts don't mess up

    - Provide screenshots of all activities involved in the program

- [A3/README.md](A3/README.md "A3 README.md")

    - Create an ERD for a theoretical petstore database in MySQL Workbench

    - Forward engineer the ERD, and add minimum 10 insert statements for each table

    - Create a native Android application that utilizes calculation and logic statements
    
- [A4/README.md](A4/README.md "A4 README.md")

    - Set up portfolio website using pre-written website code.
    
    - Prepare various pages for previously-completed assignments. 
    
    - Create a page which performs validation on the client's side using regular expressions (regex) and Bootstrap.

- [A5/README.md](A5/README.md "A5 README.md")

    - Perform server-side validation using PHP and Regex
    
    - Connect with a MySQL DB to pull and push information to a table
    
- [P1/README.md](P1/README.md "P1 README.md")

    - Create a business card application using things we have learned so far.
    
- [P2/README.md](P2/README.md "P2 README.md")

    - Iterate upon A5 by implementing edit and delete functions
    
    - Display ability to manipulate MySQL DB through PHP
    
    - Add an RSS feed for some reason