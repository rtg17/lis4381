<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Repo of software coded during 2018 LIS4381 with Mark Jowett, PhD.">
        <meta name="author" content="Rhett Gordon">
        <link rel="icon" href="favicon.ico">
        <title>LIS4381 Portfolio</title>
        <?php include_once("css/include_css.php"); ?>
        <style type="text/css">
            .item {
                height: 300px !important;
                text-align: center;
                background: #EBEBEB;
            }
            .carousel {
                margin: 20px 0px 20px 0px;
            }
            hr {
                margin: 5px;
            }
            h2
            {
                margin: 0;
                padding-top: 50px;
                font-size: 52px;
                font-family: "trebuchet ms", sans-serif;
            }
            a .carousel-link {
                text-decoration: none;
            }
            a:hover .carousel-link {
                text-decoration: none;
            }
            a:active .carousel-link {
                text-decoration: none;
            }
            a:visited .carousel-link {
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <?php include_once("global/nav_global.php"); ?>
        <div class="container">
            <div class="starter-template">
                <div class="page-header">
                    <?php include_once("global/header.php"); ?>
                </div>
                <!-- Start Bootstrap Carousel  -->
                <div id="myCarousel" class="carousel slide"
                     data-ride="carousel"
                     data-interval="7000"
                     data-pause="hover"
                     data-wrap="true"
                     data-keyboard="true">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <a class="carousel-link" target="_blank" href="https://bitbucket.org/rtg17/lis4381/src/master/">
                            <img src="images/bitbucket.png" alt="Chicago">
                            <div class="carousel-caption">
                                <h2>BITBUCKET</h2>
                                <hr>
                                <p>Everything I've done within this course is held within.</p>
                            </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="carousel-link" target="_blank" href="https://www.linkedin.com/in/rhettgordon/">
                            <img src="images/linkedin.png" alt="Chicago">
                            <div class="carousel-caption">
                                <h2>LINKEDIN</h2>
                                <hr>
                                <p>My Linkedin account, which you can connect to me with if you want.</p>
                            </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="carousel-link" target="_blank" href="https://gitlab.com/isGordon">
                            <img src="images/gitlab.png" alt="New York">
                            <div class="carousel-caption">
                                <h2>GITLAB</h2>
                                <hr>
                                <p>Another remote repo account I use for personal projects, if they come about.</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!-- End Bootstrap Carousel  -->
                <?php include_once "global/footer.php"; ?>
            </div> <!-- end starter-template -->
        </div> <!-- end container -->
        <?php include_once("js/include_js.php"); ?>
    </body>
</html>
